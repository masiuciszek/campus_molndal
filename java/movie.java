import java.util.Scanner;

public class Movie {
    static Scanner scan = new Scanner(System.in);
    static String name;
    static String[] movieList = {"1) Harry Potter", "2) Lord of the rings", "3) The dark knight"};
    static int[] movieLimits = {7, 11, 15};
    static int number;
    static int age;
    static int minAge;
    static int sale;
    static int price = 100;
    static String code;
    static String discountCode = "AJ732DD";
    static int finalPrice;
    static int adultAge;
    static int parent = 21;
    static boolean checkParant = true;
    static String secondChance;


    public static void getName(){
        System.out.println("please select your name");
        name = scan.nextLine();
        System.out.println("hello " + name);

    }

    public static boolean checkAge(int number, int age){
        boolean check = true;
        if(age < movieLimits[number -1] ){
            check = false;
        }

        return check;
    }


    public static void getAge(){
        System.out.println("please select your age");
        age = scan.nextInt();
        System.out.println("Thank you , your age is " + age);

    }


    public static void getMovie(){
        System.out.println("please select a movie");
        for(int i = 0; i < movieList.length; i++){
            System.out.println(movieList[i]);
        }
        number = scan.nextInt();
        System.out.println("Your movie that you selected was " + movieList[number -1].substring(3, movieList[number-1].length()));


    }
    public static void getTickets(Boolean check, boolean checkDiscount){
        if (check) {
            if (checkDiscount){
                 finalPrice = price -20;
            } else {
                finalPrice = price;
            }
            System.out.println("please select how many tickets you would like");
            int tickets = scan.nextInt();
            System.out.println("Thnak you master you selected " + tickets + "tickets(s), and the price you have to pay is : " + tickets * finalPrice + "swedish krones  for the movie" + movieList[number - 1].substring(3, movieList[number - 1].length()));
        } else if(checkParant)  {
            System.out.println("Do you have an adult with you ?");

            secondChance = scan.next();
            if (secondChance.equals("yes") ) {
                System.out.println("How old is the adult?");
                adultAge = scan.nextInt();
                if(adultAge >= 21){
                    System.out.println("You are allowed to watch the movie");
                } else {
                    System.out.println("Sorry you are not allowed to watch the movie. The limit age for this movie is " + movieLimits[number -1]);
                }

            } else {
                System.out.println("Sorry you are not allowed to watch the movie. The limit age for this movie is " + movieLimits[number -1]);
            }

        }
    }

    public static boolean checkCode (){
        System.out.println("enter your code");
        code = scan.next();
        if (code.equals(discountCode)){
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args){
        getName();
        getAge();
        getMovie();
        Boolean ch = checkAge(number, age);
        boolean checkDiscount = checkCode();
        getTickets(ch, checkDiscount);
    }
}